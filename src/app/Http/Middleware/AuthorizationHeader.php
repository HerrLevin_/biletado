<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\UnauthorizedException;

class AuthorizationHeader
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        try {
            $this->validateAuth(request: $request);
        } catch (UnauthorizedException $exception) {
            Log::error(message: $exception->getMessage());
            abort(code: 401);
        }
        return $next($request);
    }

    private function validateAuth(Request $request): void
    {
        if (!$request->hasHeader(key: 'Authorization')) {
            throw new UnauthorizedException(message: "No Authorization header supplied.");
        }
        $token = $request->header(key: 'Authorization');
        if (!str_starts_with(haystack: $token, needle: 'Bearer ')) {
            throw new UnauthorizedException(message: "Authorization header not to specs.");
        }

        try {
            $publicKey = $this->getPublicKeyFromKeycloak();
            JWT::decode(substr(string: $token, offset: 7), new Key(keyMaterial: $publicKey,  algorithm: 'RS256'));
            //If token is invalid, this will fail
        } catch (Exception $exception) {
            throw new UnauthorizedException(message: $exception->getMessage());
        } catch (GuzzleException $exception) {
            throw new UnauthorizedException(message: $exception->getMessage());
        }
    }

    /**
     * @throws GuzzleException
     */
    private function getPublicKeyFromKeycloak(): string {
        $client = new Client(config: [
            'base_uri' => 'http://' . env(key: 'KEYCLOAK_HOST'),
            'headers' => [
                env(key: 'JAEGER_TRACECONTEXTHEADERNAME', default: 'uber-trace-id') => session(key: 'jaeger-token')
            ]
        ]);
        $uri = new Uri(uri: "/auth/realms/" . env(key: 'KEYCLOAK_REALM'));

        $response = $client->request(
            method: 'GET',
            uri: $uri,
            options: [
                'verify'  => false,
            ]
        );

        $body = json_decode(json: $response->getBody()->getContents(), associative: true);
        return strtr(
            "-----BEGIN PUBLIC KEY-----\n:key\n-----END PUBLIC KEY-----",
            [':key' => $body['public_key']]
        );
    }
}

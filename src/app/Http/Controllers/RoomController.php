<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoomResource;
use App\Models\Room;
use App\Models\Storey;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Session\Store;

class RoomController extends Controller
{
    /**
     * Validation rules for POST/PUT requests
     * @var array|string[]
     */
    private array $validationRules = [
        'id' => 'uuid|nullable',
        'storey_id' => 'uuid|required',
        'name' => 'required',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function index(Request $request): JsonResponse|AnonymousResourceCollection
    {
        if (isset($request['storey_id'])) {
            return $this->returnRooms(Room::where('storey_id', $request['storey_id'])->get());
        }
        return $this->returnRooms(Room::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validated = $request->validate($this->validationRules);
        $status = 201;

        // Abort if supplied storey is non-existent
        if (!Storey::find($validated['storey_id'])) {
            abort(422);
        }

        if (isset($validated['id'])) {
            $room = Room::findOrFail($validated['id']);
            $room->update($validated);
            $status = 200;
        }
        return $this->returnRoom(room: Room::create($validated), statusCode: $status);
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        return $this->returnRoom(Room::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $id
     * @return void
     */
    public function update(Request $request, string $id): void
    {
        $room = Room::findOrFail($id);
        //ToDo: maybe this validation is without a required storey_id
        $validated = $request->validate($this->validationRules);

        if ((isset($validated['id']) && $id !== $validated['id']) || !Storey::find($validated['storey_id'])) {
            abort(code: 422);
        }

        $room->update($validated);
        abort(code: 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     * @return void
     */
    public function destroy(string $id): void
    {
        $room = Room::findOrFail($id);

        if ($room->reservationsCount() > 0) {
            abort(422);
        }

        $room->delete();
        abort(204);
    }

    /**
     * Return single RoomResource with an HTTP response body and predefined status code
     * @param Room $room
     * @param int $statusCode
     * @return JsonResponse
     */
    private function returnRoom(Room $room, int $statusCode=200): JsonResponse
    {
        return (new RoomResource($room))->response()->setStatusCode($statusCode);
    }

    /**
     * Return RoomResource collection with an HTTP response body and predefined status code
     * @param mixed $rooms
     * @param int $statusCode
     * @return JsonResponse
     */
    private function returnRooms(mixed $rooms, int $statusCode=200): JsonResponse
    {
        return (RoomResource::collection(resource: $rooms))->response()->setStatusCode($statusCode);
    }
}

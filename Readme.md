# Biletado asset backend

## Configuration

Default Port is `8000`

Configuration happens via environment variables:

| Key                           | example value                    | default value        | explaination                 |
|-------------------------------|----------------------------------|----------------------|------------------------------|
| APP_DEBUG                     | true                             | false                | if true, show debug messages |
| DB_CONNECTION                 | pgsql                            |                      | DB driver                    |
| DB_HOST                       | postgresql                       |                      | DB Host                      |
| DB_USERNAME                   | ${POSTGRES_USERNAME}             |                      | DB Username                  |
| DB_PASSWORD                   | ${POSTGRES_PASSWORD}             |                      | DB Password                  |
| DB_DATABASE                   | assets                           |                      | DB Name                      |
| DB_PORT                       | 5432                             |                      | DB Port                      |
| RESERVATIONS_HOST             | backend-reservations             | backend-reservations | Host for reservations api    |
| RESERVATIONS_PORT             | 9000                             | 9000                 | Port for reservations api    |
| KEYCLOAK_HOST                 | traefik                          |                      |                              |
| KEYCLOAK_REALM                | biletado                         |                      |                              |
| JAEGER_TRACECONTEXTHEADERNAME | ${JAEGER_TRACECONTEXTHEADERNAME} |                      |                              |
FROM composer:2 as ComposerBuildContainer

WORKDIR /var/www

COPY ./src /var/www

RUN composer install --ignore-platform-reqs --optimize-autoloader --no-dev

#From https://learn2torials.com/a/laravel8-production-docker-image
FROM php:8.1-fpm

WORKDIR /var/www

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN apt-get update -y && apt-get install -y \
    build-essential \
    libmcrypt-dev \
    zlib1g-dev \
    libpq-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    unzip \
    git \
    curl \
    lua-zlib-dev \
    libmemcached-dev \
    nginx


RUN docker-php-ext-install pdo pdo_pgsql pgsql
RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql


# Install supervisor
RUN apt-get install -y supervisor
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy code to /var/www
COPY --from=ComposerBuildContainer --chown=www:www-data /var/www /var/www

# Copy nginx/php/supervisor configs
COPY ./docker/php.ini /usr/local/etc/php/conf.d/app.ini
COPY ./docker/supervisor.conf /etc/supervisord.conf
COPY ./docker/nginx.conf /etc/nginx/sites-enabled/default
COPY --chown=www:www-data ./docker/run.sh /var/www/docker/run.sh

# PHP Error Log Files
RUN mkdir /var/log/php
RUN touch /var/log/php/errors.log && chmod 777 /var/log/php/errors.log
RUN mkdir -p /var/log/nginx && touch /var/log/nginx/access.log && touch /var/log/nginx/error.log

RUN find /var/www/ -type f -exec chmod 664 {} \;
RUN find /var/www/ -type d -exec chmod 775 {} \;

RUN chmod +x /var/www/docker/run.sh

EXPOSE 8000
ENTRYPOINT ["/var/www/docker/run.sh"]
<?php

namespace Tests\Unit;

use Tests\TestCase;

class AuthorizationHeaderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testFailOnNoGivenAuthorizationHeader(): void
    {
        $response = $this->post(uri: '/api/assets/storeys');

        $response->assertStatus(status: 401);
    }
}

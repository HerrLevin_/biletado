<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\Pure;
use stdClass;

class JsonErrorResource extends JsonResource
{
    #[Pure] public function __construct($resource = [])
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'message' => $this->message ?? 'string',
            'additionalProp1' => $this->additionalProp1 ?? new stdClass()
        ];
    }
}
